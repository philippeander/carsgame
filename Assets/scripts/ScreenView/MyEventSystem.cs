﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MyEventSystem : EventSystem {

	protected override void OnEnable(){
		// do not assign EventSystem.current
	}

	protected override void Update()
	{
		EventSystem originalCurrent = EventSystem.current;
		current = this; // in order to avoid reimplementing half of the EventSystem class, just temporarily assign this EventSystem to be the globally current one
		base.Update();
		current = originalCurrent;
	}

}

public class MyButton : Button
{
	public EventSystem eventSystem;

	public override void OnPointerDown(PointerEventData eventData)
	{
		if (eventData.button != PointerEventData.InputButton.Left)
			return;

		// Selection tracking
		if (IsInteractable() && navigation.mode != Navigation.Mode.None)
			eventSystem.SetSelectedGameObject(gameObject, eventData);

		base.OnPointerDown(eventData);
	}

	public override void Select()
	{
		if (eventSystem.alreadySelecting)
			return;

		eventSystem.SetSelectedGameObject(gameObject);
	}
}
