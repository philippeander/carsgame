﻿using System;
using UnityEngine.UI;
using System.Linq;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;


namespace scripts{

	public class Menu : MonoBehaviour {

		[SerializeField]private Transform arrow;


		[Header("Buttons")]
		[SerializeField]private string confirmsButton = "Action 2_P1";

		public static int modePlayer = 0;

		// Use this for initialization
		void Start () {
			
		}

		// Update is called once per frame
		void Update () {
			
		} 
			

		public void SelectModePlayer(int IdMode){
			if (Input.GetButtonDown (confirmsButton)) {
				modePlayer = IdMode;
				SceneManager.LoadScene ("CarSelected");
			}
		}

		public void MoveArrow(Transform target){
			arrow.transform.position = target.transform.position;
		}
	}
}