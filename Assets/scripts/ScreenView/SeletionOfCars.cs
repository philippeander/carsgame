﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//Script para visualização e seleção dos carros

namespace scripts{

	//Essa classe é chamada dentro do ARRAY "carByPlayer" e está relacionada a todas informações mostradas na tela do player à ela relacionada
	[Serializable]
	public class CarByPlayer{

		[Header("CONFIG.")]
		public int IdPlayer = 0;  				//Identificação do player
		public int carViewTurn = 0;				//Numero do Indiçe referente ao carro a ser mostrado
		public Transform pointInstantiate;		//Ponto onde instancia o carro
		public GameObject carView;				//Carro instanciado da vez
		public bool m_Finished = false;					//Mostra se o player já finalizou a escolha

		[Space(10)]
		public GameObject PlatformRotate;		//Rotação da plataforma do carro

		[Header("INPUT BUTTON BY PLAYER")]
		public string leftCarChenge = "Action 6_P1";	//botão de escolha dos carros
		public string RightCarChenge = "Action 7_P1";	//botão de escolha dos carros


	}

	public class SeletionOfCars : MonoBehaviour {

		[Header("PLAYER INFO")]
		[SerializeField]private List<CarByPlayer> carByPlayer = new List<CarByPlayer>();

		[Header("CARs LIST")]
		[SerializeField]private List<GameObject> CarsList = new List<GameObject>(); //Lista de carros para a visualização do player

		[Header("ROTATE VELOCITY")]
		[SerializeField][Range(0,10)]private float speedPlatform = 5f; 				//Controle de rotação da plataforma do carro...

		//Abaixo valores estaticos para informar na proxima cena qual carro a ser instanciado, trabalhando em conjunto com a variavel "modePlayer" do script "Menu"
		public static int CarPl_1;		
		public static int CarPl_2;
		public static int CarPl_3;
		public static int CarPl_4;


		void Awake(){

		}

		// Use this for initialization
		void Start () {
			Countdown.rbPause = true;
			foreach (CarByPlayer CP in carByPlayer) {
				
				GameObject InstCar = Instantiate(CarsList[CP.carViewTurn], CP.pointInstantiate.position, Quaternion.Euler(0,150f,0)) as GameObject;
				CP.carView = InstCar;
			}

		}

		// Update is called once per frame
		void Update () {

			CarPl_1 = carByPlayer [0].carViewTurn;
			CarPl_2 = carByPlayer [1].carViewTurn;
			CarPl_3 = carByPlayer [2].carViewTurn;
			CarPl_4 = carByPlayer [3].carViewTurn;

			SelectionMyCar ();
			VerifyIfAllChoose ();

			//Abaixo o motor de rotação para girar a plataforma e o carro
			//Foi colocado um para o carro também porque a plataforma estava girando e o carro não...
			//Como os dois recebem o mesmo parametro de velocidade, não causa estranhesa no movimento
			foreach (CarByPlayer CP in carByPlayer) {
				CP.carView.transform.Rotate(Vector3.up * Time.deltaTime * speedPlatform, Space.World);
				CP.PlatformRotate.transform.Rotate(Vector3.up * Time.deltaTime * speedPlatform, Space.World);
			}

		}


		private void SelectionMyCar(){ 

			foreach (CarByPlayer CP in carByPlayer) { 
				
				if (Input.GetButtonDown(CP.leftCarChenge) || Input.GetKeyUp(KeyCode.D) && CP.IdPlayer == 1) {
					Destroy (CP.carView);
					CP.carViewTurn += 1;
					if (CP.carViewTurn > CarsList.Count - 1)
						CP.carViewTurn = 0;
					if (CP.carViewTurn < 0)
						CP.carViewTurn = CarsList.Count - 1;
					GameObject InstCar = Instantiate(CarsList[CP.carViewTurn], CP.pointInstantiate.position, Quaternion.Euler(0,150f,0)) as GameObject;
					CP.carView = InstCar;

				} 

				if (Input.GetButtonDown(CP.RightCarChenge) || Input.GetKeyUp(KeyCode.A) && CP.IdPlayer == 1 ) {
					Destroy (CP.carView);
					CP.carViewTurn -= 1;
					if (CP.carViewTurn > CarsList.Count - 1) 
						CP.carViewTurn = 0;
					if (CP.carViewTurn < 0)
						CP.carViewTurn = CarsList.Count - 1;
					GameObject InstCar = Instantiate(CarsList[CP.carViewTurn], CP.pointInstantiate.position, Quaternion.Euler(0,150f,0)) as GameObject;
					CP.carView = InstCar;
				
				}
			}
		}

		//script temporario
		public void VerifyIfAllChoose(){
			if (Menu.modePlayer == 1) {
				if (carByPlayer [0].m_Finished == true) {
					GoGame ();
				}
			}

			if (Menu.modePlayer == 2) {
				if (carByPlayer [0].m_Finished == true &&
					carByPlayer [1].m_Finished == true) {
					GoGame ();
				}
			}

			if (Menu.modePlayer == 3) {
				if (carByPlayer [0].m_Finished == true &&
					carByPlayer [1].m_Finished == true &&
					carByPlayer [2].m_Finished == true) {
					GoGame ();
				}
			}

			if (Menu.modePlayer == 4) {
				if (carByPlayer [0].m_Finished == true &&
					carByPlayer [1].m_Finished == true &&
					carByPlayer [2].m_Finished == true &&
					carByPlayer [3].m_Finished == true) {
					GoGame ();
				}
			}
			
		}

		public void GoGame(){
			SceneManager.LoadScene ("Game");
		}

		public void BtnFinished(int CamIdPlayer){

			if (CamIdPlayer == 1) {
				if (Input.GetButtonDown ("Action 2_P1")) {
					carByPlayer [0].m_Finished = true;
				}
			}
			if (CamIdPlayer == 2) {
				if (Input.GetButtonDown ("Action 2_P2")) {
					carByPlayer [1].m_Finished = true;
				}
			}
			if (CamIdPlayer == 3) {
				if (Input.GetButtonDown ("Action 2_P3")) {
					carByPlayer [2].m_Finished = true;
				}
			}
			if (CamIdPlayer == 4) {
				if (Input.GetButtonDown ("Action 2_P4")) {
					carByPlayer [3].m_Finished = true;
				}
			}
		}


		public void ButtonSelect(GameObject Btn){
			Image myBtnImage = Btn.GetComponent<Image> ();
			myBtnImage.enabled = true;
		}

		public void ButtonDeselect(GameObject Btn){
			Image myBtnImage = Btn.GetComponent<Image> ();
			myBtnImage.enabled = false;
		}

	}
}