﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic; // To "list"
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

//Script para o Splet Screen do menu...

namespace scripts{
	//Essa classe é chamada dentro do ARRAY "initialButtons" e permite Ativar os botões principais, para utilização dos 
	//joysticks, de acordo com o modo de cameras ativo... 
	[Serializable]
	public class FirstButtons{
		public List<GameObject> ButtonByScene = new List<GameObject> ();
	}


	public class CameraManeger : MonoBehaviour {

		[Header("Initial Button")]
		[SerializeField]private List<FirstButtons> initialButtons = new List<FirstButtons>();

		[Header("Config.")]
		[SerializeField]private List<GameObject> Cameras = new List<GameObject>();				//Ativa o Game Object relacionado a função de Acordo com o numero de players selecionado no Menu
		[SerializeField]private List<MyEventSystem> MyArrayES = new List<MyEventSystem> ();		//Pega o script "EVENT SYSTEM" relacionado ao controle, para ativar o botão principal
		[SerializeField]private string Esc = "Action 8_P1";										//Botão para retornar a cena anterior.


		void Awake(){

			//Esse quadro abaixo seta o splet screen e os botões principais de cada camera..
			if (Menu.modePlayer == 1) {
				Cameras [0].SetActive (true);
				MyArrayES [0].SetSelectedGameObject (initialButtons[0].ButtonByScene[0]);
			}
			else{
				Cameras [0].SetActive (false); 
			}

			if (Menu.modePlayer == 2) {
				Cameras [1].SetActive (true);
				MyArrayES [0].SetSelectedGameObject (initialButtons[1].ButtonByScene[0]);
				MyArrayES [1].SetSelectedGameObject (initialButtons[1].ButtonByScene[1]);
			} else { 
				Cameras [1].SetActive (false);
			}

			if (Menu.modePlayer == 3) {
				Cameras [2].SetActive (true);
				MyArrayES [0].SetSelectedGameObject (initialButtons[2].ButtonByScene[0]);
				MyArrayES [1].SetSelectedGameObject (initialButtons[2].ButtonByScene[1]);
				MyArrayES [2].SetSelectedGameObject (initialButtons[2].ButtonByScene[2]);
			} else { 
				Cameras [2].SetActive (false);
			}

			if (Menu.modePlayer == 4) {
				Cameras [3].SetActive (true);
				MyArrayES [0].SetSelectedGameObject (initialButtons[3].ButtonByScene[0]);
				MyArrayES [1].SetSelectedGameObject (initialButtons[3].ButtonByScene[1]);
				MyArrayES [2].SetSelectedGameObject (initialButtons[3].ButtonByScene[2]);
				MyArrayES [3].SetSelectedGameObject (initialButtons[3].ButtonByScene[3]);
			} else { 
				Cameras [3].SetActive (false);

			}
		}

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
			if (Input.GetButtonDown (Esc))
				SceneManager.LoadScene ("Menu");
		}
	}
}