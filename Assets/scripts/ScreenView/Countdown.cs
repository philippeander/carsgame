﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

namespace scripts{

	public class Countdown : MonoBehaviour {

		[SerializeField]private int countMax = 3;
		[SerializeField]private Text visualCount;

		private int countDown;
		public static bool rbPause;

		void Start () {
			rbPause = true;
			StartCoroutine (GameStart());

		}
		

		void Update () {
			
		}

		private IEnumerator GameStart(){
			for (countDown = countMax; countDown > -1; countDown--) {
				if (countDown != 0) {
					visualCount.text = countDown.ToString ();
				} else {
					visualCount.text = "GO!";
					rbPause = false;
					yield return new WaitForSeconds (1.5f);
					visualCount.enabled = false;
				}
				yield return new WaitForSeconds (1.5f);
			}
		}
			
	}
}