﻿using UnityEngine;
using System.Collections;

namespace scripts{

	public class PlayerCamera : MonoBehaviour {


		public Transform playerCar;
		private Rigidbody playerRigid;
		private Camera cam;


		public float distance = 6.0f;


		public float height = 2.0f;

		private float defaultHeight = 0f;

		public float heightOffset = .75f;
		public float heightDamping = 2.0f;
		public float rotationDamping = 3.0f;

		public float minimumFOV = 50f;
		public float maximumFOV = 70f;

		public float maximumTilt = 15f;
		private float tiltAngle = 0f;

		void Start(){



			playerRigid = playerCar.GetComponent<Rigidbody>();
			cam = GetComponent<Camera>();



		}

		void Update(){


			if (!playerCar)
				return;

			if(playerRigid != playerCar.GetComponent<Rigidbody>())
				playerRigid = playerCar.GetComponent<Rigidbody>();

			//Calculod do angulo de inclinação
			tiltAngle = Mathf.Lerp (tiltAngle, (Mathf.Clamp (-playerCar.InverseTransformDirection(playerRigid.velocity).x, -35, 35)), Time.deltaTime * 2f);

			if(!cam)
				cam = GetComponent<Camera>();

			cam.fieldOfView = Mathf.Lerp (minimumFOV, maximumFOV, (playerRigid.velocity.magnitude * 3f) / 150f);

		}

		void LateUpdate (){

			// Checa se tem um alvo
			if (!playerCar || !playerRigid)
				return;

			float speed = (playerRigid.transform.InverseTransformDirection(playerRigid.velocity).z) * 3f;

			// Calcular os ângulos de rotação atuais.
			float wantedRotationAngle = playerCar.eulerAngles.y;
			float wantedHeight = playerCar.position.y + height;
			float currentRotationAngle = transform.eulerAngles.y;
			float currentHeight = transform.position.y;

			if(speed < -2)
				wantedRotationAngle = playerCar.eulerAngles.y + 180;

			// Damp a rotação em torno do eixo y
			currentRotationAngle = Mathf.LerpAngle (currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);

			// Damp a altura
			currentHeight = Mathf.Lerp (currentHeight, wantedHeight, heightDamping * Time.deltaTime);

			// converte o angulo para uma rotação
			Quaternion currentRotation = Quaternion.Euler (0, currentRotationAngle, 0);

			// Defina a posição da câmera no plano x-z para:
			// medidores de distância por trás do alvo
			transform.position = playerCar.position;
			transform.position -= currentRotation * Vector3.forward * distance;

			// Defina a altura da câmera
			transform.position = new Vector3(transform.position.x, currentHeight + defaultHeight, transform.position.z);

			// Sempre olhar para o alvo
			transform.LookAt (new Vector3(playerCar.position.x, playerCar.position.y + heightOffset, playerCar.position.z));
			transform.eulerAngles = new Vector3(transform.eulerAngles.x,transform.eulerAngles.y, Mathf.Clamp(tiltAngle, -10f, 10f));

		}

	}
}