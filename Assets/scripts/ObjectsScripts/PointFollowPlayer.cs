﻿using UnityEngine;
using System.Collections;

//Essa classe ta relacionada ao minimapa, ele é o ponto que mostra a localização do player no Mini-Mapa

public class PointFollowPlayer : MonoBehaviour {


	public int ControlID = 0;
	public GameObject goPlayer;

	[SerializeField]private Color[] ArrowColor; 

	// Use this for initialization
	void Start () {
	
		//goPlayer = GameObject.FindGameObjectWithTag ("Player");
		if(ControlID == 1)
			GetComponent<SpriteRenderer>().color = ArrowColor[0];
		else if(ControlID == 2)
			GetComponent<SpriteRenderer>().color = ArrowColor[1];
		else if(ControlID == 3)
			GetComponent<SpriteRenderer>().color = ArrowColor[2];
		else if(ControlID == 4)
			GetComponent<SpriteRenderer>().color = ArrowColor[3]; 
		

	} 
	
	// Update is called once per frame
	void Update () {
	
		transform.position = new Vector3 (goPlayer.transform.position.x, transform.position.y, goPlayer.transform.position.z) ;
		transform.eulerAngles = new Vector3 (transform.eulerAngles.x, goPlayer.transform.eulerAngles.y - 90f, transform.eulerAngles.z);
	}
}
