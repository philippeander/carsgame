﻿using UnityEngine;
using System.Collections;

namespace scripts{
	
	public class RepeatedMovements : MonoBehaviour {
		
		[Header("Sine Moviment")]
		public float delta = 1.5f; 
		public float speed = 2.0f; 
		private Vector3 startPos;

		[Header("Mesh Renderer")]
		public MeshRenderer MH;
		public int timeMesh = 5;

		void Start () {
			startPos = transform.position;
			MH = GetComponent<MeshRenderer> ();
		}

		void Update () {
			Vector3 v = startPos;
			v.y += delta * Mathf.Sin (Time.time * speed);
			transform.position = v;

			StartCoroutine (ActiveMesh());

		}

		private IEnumerator ActiveMesh(){
		
			if(MH.enabled == false){

				yield return new WaitForSeconds (timeMesh);

				MH.enabled = true;
			}
		}
	}
}