﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;
using UnityStandardAssets.ImageEffects;
using UnityEngine.UI;

namespace scripts{

	[Serializable]
	public class CamByPlayer{
		
		public GameObject[] camerascript;								//Essa classe permite pegar o script de todas as cameras

	}

	[RequireComponent(typeof(PlayerCamera))]
	public class GameManager : MonoBehaviour {
		[Header("TUTO")]
		[SerializeField]private GameObject CtrlInfo;
		[SerializeField]private bool isPaused;
		[SerializeField]private Button btnPauseSelected;

		[Header("POINTS TO SPOWN CARS")]
		[SerializeField]private GameObject[] pointsSpown;				//Pontos de instanciação dos carros

		[Header("CARS")]
		[SerializeField]private GameObject[] Cars;						//Tem que setar os carros na mesma ordem da sena de seleção de carros, para instanciar corretamente
		[SerializeField]private GameObject[] carsWithAi;				//Carros com inteligencia Artificial a serem instanciados na cena.

		[Header("Spleet Group")]
		[SerializeField]private CamByPlayer[] camByPlayer = null;		//Array referente a Classe criada acima
		[SerializeField]private GameObject[] Cameras;					//Spleet que deverá ser ativo


		void Start() {
		
			//isPaused = true;
			Debug.Log (SeletionOfCars.CarPl_1 + "\n"+
						SeletionOfCars.CarPl_2 + "\n"+
						SeletionOfCars.CarPl_3 + "\n"+
						SeletionOfCars.CarPl_4 );

			if (Menu.modePlayer == 1) { // Se o modo Player for igual a 1 Jogador
				Cameras [0].SetActive (true); //Splet Group ativo
				GameObject newCar1 = Instantiate (Cars [SeletionOfCars.CarPl_1], pointsSpown [3].transform.position, Quaternion.identity) as GameObject;
				newCar1.name = Cars [SeletionOfCars.CarPl_1].name;
				camByPlayer [0].camerascript [0].GetComponent<PlayerCamera>().playerCar = newCar1.transform; 

				CarControllerInput CCI_1 = newCar1.GetComponent<CarControllerInput> ();
				CCI_1.CarId = 1;

				CarNitro CN_1 = newCar1.GetComponent<CarNitro> ();
				CN_1.m_camera = camByPlayer [0].camerascript [0].GetComponent<Camera>();

				//Por enquanto sempre seram colocados até quatro jogadores indempendentese é com IA ou não
				GameObject Ai_1 = Instantiate (carsWithAi[0], pointsSpown [0].transform.position, Quaternion.identity) as GameObject;
				GameObject Ai_2 = Instantiate (carsWithAi[0], pointsSpown [1].transform.position, Quaternion.identity) as GameObject;
				GameObject Ai_3 = Instantiate (carsWithAi[0], pointsSpown [2].transform.position, Quaternion.identity) as GameObject;
				Ai_1.GetComponent<CarAiController> ().CarId = 2;
				Ai_2.GetComponent<CarAiController> ().CarId = 3;
				Ai_3.GetComponent<CarAiController> ().CarId = 4;




			} else {
				Cameras [0].SetActive (false);
			}

			if(Menu.modePlayer == 2){
				Cameras [1].SetActive (true);
				GameObject newCar1 = Instantiate(Cars[SeletionOfCars.CarPl_1], pointsSpown[2].transform.position, Quaternion.identity) as GameObject;
				newCar1.name = Cars [SeletionOfCars.CarPl_1].name;
				camByPlayer[1].camerascript[0].GetComponent<PlayerCamera>().playerCar = newCar1.transform; 

				GameObject newCar2 = Instantiate(Cars[SeletionOfCars.CarPl_2], pointsSpown[3].transform.position, Quaternion.identity) as GameObject;
				newCar2.name = Cars [SeletionOfCars.CarPl_2].name;
				camByPlayer[1].camerascript[1].GetComponent<PlayerCamera>().playerCar = newCar2.transform;

				CarControllerInput CCI_1 = newCar1.GetComponent<CarControllerInput> ();
				CarControllerInput CCI_2 = newCar2.GetComponent<CarControllerInput> ();
				CCI_1.CarId = 1;
				CCI_2.CarId = 2;

				CarNitro CN_1 = newCar1.GetComponent<CarNitro> ();
				CarNitro CN_2 = newCar2.GetComponent<CarNitro> ();
				CN_1.m_camera = camByPlayer [1].camerascript [0].GetComponent<Camera>();
				CN_2.m_camera = camByPlayer [1].camerascript [1].GetComponent<Camera>();

				GameObject Ai_1 = Instantiate (carsWithAi[0], pointsSpown [0].transform.position, Quaternion.identity) as GameObject;
				GameObject Ai_2 = Instantiate (carsWithAi[0], pointsSpown [1].transform.position, Quaternion.identity) as GameObject;
				Ai_1.GetComponent<CarAiController> ().CarId = 3;
				Ai_2.GetComponent<CarAiController> ().CarId = 4;


			}else {
				Cameras [1].SetActive (false);
			}

			if(Menu.modePlayer == 3){
				Cameras [2].SetActive (true);
				GameObject newCar1 = Instantiate(Cars[SeletionOfCars.CarPl_1], pointsSpown[0].transform.position, Quaternion.identity) as GameObject;
				newCar1.name = Cars [SeletionOfCars.CarPl_1].name;
				camByPlayer[2].camerascript[0].GetComponent<PlayerCamera>().playerCar = newCar1.transform;

				GameObject newCar2 = Instantiate(Cars[SeletionOfCars.CarPl_2], pointsSpown[1].transform.position, Quaternion.identity) as GameObject;
				newCar2.name = Cars [SeletionOfCars.CarPl_2].name;
				camByPlayer[2].camerascript[1].GetComponent<PlayerCamera>().playerCar = newCar2.transform;

				GameObject newCar3 = Instantiate(Cars[SeletionOfCars.CarPl_3], pointsSpown[2].transform.position, Quaternion.identity) as GameObject;
				newCar3.name = Cars [SeletionOfCars.CarPl_3].name;
				camByPlayer[2].camerascript[2].GetComponent<PlayerCamera>().playerCar = newCar3.transform;

				CarControllerInput CCI_1 = newCar1.GetComponent<CarControllerInput> ();
				CarControllerInput CCI_2 = newCar2.GetComponent<CarControllerInput> ();
				CarControllerInput CCI_3 = newCar3.GetComponent<CarControllerInput> ();
				CCI_1.CarId = 1;
				CCI_2.CarId = 2;
				CCI_3.CarId = 3;

				CarNitro CN_1 = newCar1.GetComponent<CarNitro> ();
				CarNitro CN_2 = newCar2.GetComponent<CarNitro> ();
				CarNitro CN_3 = newCar3.GetComponent<CarNitro> ();
				CN_1.m_camera = camByPlayer [2].camerascript [0].GetComponent<Camera>();
				CN_2.m_camera = camByPlayer [2].camerascript [1].GetComponent<Camera>();
				CN_3.m_camera = camByPlayer [2].camerascript [2].GetComponent<Camera>();

				GameObject Ai_1 = Instantiate (carsWithAi[0], pointsSpown [3].transform.position, Quaternion.identity) as GameObject;
				Ai_1.GetComponent<CarAiController> ().CarId = 4;


			}else {
				Cameras [2].SetActive (false);
			}

			if(Menu.modePlayer == 4){
				Cameras [3].SetActive (true);
				GameObject newCar1 = Instantiate(Cars[SeletionOfCars.CarPl_1], pointsSpown[1].transform.position, Quaternion.identity) as GameObject;
				newCar1.name = Cars [SeletionOfCars.CarPl_1].name;
				camByPlayer[3].camerascript[0].GetComponent<PlayerCamera>().playerCar = newCar1.transform; 

				GameObject newCar2 = Instantiate(Cars[SeletionOfCars.CarPl_2], pointsSpown[2].transform.position, Quaternion.identity) as GameObject;
				newCar2.name = Cars [SeletionOfCars.CarPl_2].name;
				camByPlayer[3].camerascript[1].GetComponent<PlayerCamera>().playerCar = newCar2.transform; 

				GameObject newCar3 = Instantiate(Cars[SeletionOfCars.CarPl_3], pointsSpown[3].transform.position, Quaternion.identity) as GameObject;
				newCar3.name = Cars [SeletionOfCars.CarPl_3].name;
				camByPlayer[3].camerascript[2].GetComponent<PlayerCamera>().playerCar = newCar3.transform; 

				GameObject newCar4 = Instantiate(Cars[SeletionOfCars.CarPl_4], pointsSpown[4].transform.position, Quaternion.identity) as GameObject;
				newCar4.name = Cars [SeletionOfCars.CarPl_4].name;
				camByPlayer[3].camerascript[3].GetComponent<PlayerCamera>().playerCar = newCar4.transform; 

				CarControllerInput CCI_1 = newCar1.GetComponent<CarControllerInput> ();
				CarControllerInput CCI_2 = newCar2.GetComponent<CarControllerInput> ();
				CarControllerInput CCI_3 = newCar3.GetComponent<CarControllerInput> ();
				CarControllerInput CCI_4 = newCar4.GetComponent<CarControllerInput> ();
				CCI_1.CarId = 1;
				CCI_2.CarId = 2;
				CCI_3.CarId = 3;
				CCI_3.CarId = 4;

				CarNitro CN_1 = newCar1.GetComponent<CarNitro> ();
				CarNitro CN_2 = newCar2.GetComponent<CarNitro> ();
				CarNitro CN_3 = newCar3.GetComponent<CarNitro> ();
				CarNitro CN_4 = newCar4.GetComponent<CarNitro> ();
				CN_1.m_camera = camByPlayer [3].camerascript [0].GetComponent<Camera>();
				CN_2.m_camera = camByPlayer [3].camerascript [1].GetComponent<Camera>();
				CN_3.m_camera = camByPlayer [3].camerascript [2].GetComponent<Camera>();
				CN_4.m_camera = camByPlayer [3].camerascript [3].GetComponent<Camera>();


			}else {
				Cameras [3].SetActive (false);
			}





		
		}

		void Update(){
			MyPause ();
			 
		}

		private void MyPause(){
			if(Input.GetButtonDown("Action 9_P1") || Input.GetButtonDown("Action 9_P2") || Input.GetButtonDown("Action 9_P3") || Input.GetButtonDown("Action 9_P4") || Input.GetKeyDown(KeyCode.Escape) ){
				isPaused = !isPaused;
			}


			if (isPaused) {
				CtrlInfo.SetActive (true);
				Time.timeScale = 0;
				btnPauseSelected.Select ();

			} else {
				Time.timeScale = 1;
				CtrlInfo.SetActive (false);
			}
		}

		public void MyContinue(){
			isPaused = !isPaused;
		}

		public void MyQuit(){
			Time.timeScale = 1;
			SceneManager.LoadScene (0);
		}

	}
}