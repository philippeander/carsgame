﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;


namespace scripts{

	internal enum Mode{

		Acceleration, 
		Impulse 
	}

	[RequireComponent(typeof(CarController))]
	[RequireComponent(typeof(CameraMotionBlur))]
	public class CarNitro : MonoBehaviour {

		public int ControlId = 0;

		[Header("Accel & Impulse System")]
		[SerializeField]private Mode mode = Mode.Acceleration;
		[SerializeField]private float value = 10.0f;
		[SerializeField]private float maxVelocity = 80.0f;
		[SerializeField]private KeyCode key = KeyCode.I;
		[SerializeField]private KeyCode keyRack = KeyCode.N;
		[SerializeField]private string m_BtnNitro = "Action 0_P";

		[Header("Particle system")]
		[SerializeField]private GameObject[] nitroParticle = null;
		[SerializeField]private float timePaticleInSec = 3.0f;
		public bool ActiveNitro = false;

		[Header("Camera Effect")]
		public Camera m_camera;
		public CameraMotionBlur CMBlur;


		[Header("UI")]
		public GameObject IconNitro; 

		public bool m_activeNitro{get{ return ActiveNitro; }}

		public int cont = 0;

		Rigidbody m_rigidbody;


		void Start () 
		{
			CMBlur = m_camera.GetComponent<CameraMotionBlur> ();
			m_rigidbody = GetComponent<Rigidbody>();
			ControlId = GetComponent<CarControllerInput>().CarId;
			IconNitro = m_camera.transform.Find("Parallax/NitroImage").gameObject;

			CMBlur.enabled = false;  
		}

		void Update ()
		{
//			MyJump ();
			if (mode == Mode.Impulse)
			{
				if (Input.GetKeyDown (keyRack) && m_rigidbody.velocity.magnitude < maxVelocity) {
				
					UpdateImpulse ();

					cont ++;
				}
			}

			if (ActiveNitro)
				IconNitro.SetActive (true);
			else
				IconNitro.SetActive (false);
		}


		void FixedUpdate ()
		{
			if (mode == Mode.Acceleration)
			{
				if (Input.GetKey (keyRack) && m_rigidbody.velocity.magnitude < maxVelocity) {
					
					FixedUpdateAccel ();
				}
			}

			if (ActiveNitro)
			{
				if (Input.GetButton (m_BtnNitro+ControlId.ToString()) && m_rigidbody.velocity.magnitude < maxVelocity) {
					FixedUpdateAccel ();
					ActiveNitro = false;
				}

				if (ControlId == 1 && Input.GetKeyDown (KeyCode.O) && m_rigidbody.velocity.magnitude < maxVelocity) {
					FixedUpdateAccel ();
					ActiveNitro = false;
				}
					
			}
		}


		//================================================================================
		//================================================================================



		void OnTriggerEnter( Collider other )
		{

			//if(other.gameObject.tag == "ImpulsePlatforme" )
			if(CMBlur.enabled == false && other.gameObject.tag == "ImpulsePlatforme" )
			{

				UpdateImpulse ();
				cont ++;
				Debug.Log ("OBL");

			}

			if(other.gameObject.tag == "waterObstacle")
			{
				Destroy(other.gameObject);
			}

			if(other.gameObject.tag == "BoxSurprise" && other.gameObject.GetComponent<MeshRenderer>().enabled == true )
			{
				ActiveNitro = true;
				other.gameObject.GetComponent<MeshRenderer>().enabled = false;
				//Destroy(other.gameObject);
			}

		}

//		private void MyJump(){
//		
//			if (Input.GetKeyDown (KeyCode.Space)) {
//			
//				m_rigidbody.AddForce (new Vector3(0, 100 ,0), ForceMode.Impulse);
//			}
//		}

		private void UpdateImpulse(){
		
			m_rigidbody.AddRelativeForce (Vector3.forward * (value / 2f), ForceMode.VelocityChange);
			StartCoroutine (ActiveCameraMotionBluer());
		}

		private void FixedUpdateAccel(){
		
			m_rigidbody.AddRelativeForce (Vector3.forward * value, ForceMode.Acceleration);
			StartCoroutine(ActiveParticle ());
			StartCoroutine (ActiveCameraMotionBluer());
		}


		//------------------------------------------------------------------------------------


		private IEnumerator ActiveParticle(){

			foreach(GameObject particle in nitroParticle ){

				particle.SetActive (true);

				yield return new WaitForSeconds (timePaticleInSec);

				particle.SetActive (false);

			}
		}

		private IEnumerator ActiveCameraMotionBluer(){

			CMBlur.enabled = true;

			yield return new WaitForSeconds (timePaticleInSec);

			CMBlur.enabled = false;

		}
	}
}