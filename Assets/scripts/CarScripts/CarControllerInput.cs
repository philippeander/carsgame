﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;


namespace scripts{

	[RequireComponent(typeof(CarController))] //Essa função exige que esse script esteja no mesmo objeto
	public class CarControllerInput : MonoBehaviour {

		public int CarId = 0;

		public string m_Vertical = "Vertical_P";
		public string m_Horizontal = "Horizontal_P";
		public string m_BtnAccel = "Action 2_P";
		public string m_BtnFootBrake = "Action 3_P";
		public string m_BtnHandGrake = "Action 7_P";
		public string m_BtnJump = "Action 1_P";

		[SerializeField]private GameObject pointPlayer;

		private CarController m_Car;
		private float accel;
		private float footbrake;
		private float handBrake;
		public bool isJump;
		// Use this for initialization
		void Start () {

			isJump = false;
			m_Car = GetComponent<CarController> ();
			Debug.Log (m_BtnAccel+CarId.ToString());

			GameObject pp =  Instantiate (pointPlayer, pointPlayer.transform.position, pointPlayer.transform.rotation) as GameObject;
			pp.GetComponent<PointFollowPlayer> ().goPlayer = this.gameObject; 
			pp.GetComponent<PointFollowPlayer> ().ControlID = this.CarId;
		}




		// Update is called once per frame
		void FixedUpdate () {
			//float accel;
			//detectPressedKeyOrButton();

			if (Input.GetButtonDown (m_BtnAccel+CarId.ToString()))
				accel = 1;
			else if(Input.GetButtonUp (m_BtnAccel+CarId.ToString()))
				accel = 0;

			if (Input.GetButtonDown (m_BtnFootBrake+CarId.ToString()))
				footbrake = -1;
			else if(Input.GetButtonUp (m_BtnFootBrake+CarId.ToString()))
				footbrake = 0;

			if (Input.GetButtonDown (m_BtnHandGrake+CarId.ToString()))
				handBrake = 1;
			else if(Input.GetButtonUp (m_BtnHandGrake+CarId.ToString()))
				handBrake = 0;

			if (Input.GetButtonDown (m_BtnJump + CarId.ToString ()))
				isJump = true;
			else if (Input.GetButtonUp (m_BtnJump + CarId.ToString ()))
				isJump = false;

			if (CarId == 1) {
				if (Input.GetKeyDown (KeyCode.J))
					accel = 1;
				else if(Input.GetKeyUp (KeyCode.J))
					accel = 0;

				if (Input.GetKeyDown (KeyCode.K))
					footbrake = -1;
				else if(Input.GetKeyUp (KeyCode.K))
					footbrake = 0;

				if (Input.GetKeyDown (KeyCode.I))
					handBrake = 1;
				else if(Input.GetKeyUp (KeyCode.I))
					handBrake = 0;

				if (Input.GetKeyDown (KeyCode.Space))
					isJump = true;
				else if (Input.GetKeyUp (KeyCode.Space))
					isJump = false;
			}
			
			float h = CrossPlatformInputManager.GetAxis (m_Horizontal+CarId.ToString());
			//float v = CrossPlatformInputManager.GetAxis ("Vertical");

			//float handbrake = CrossPlatformInputManager.GetAxis ("Jump");

			m_Car.Engine (accel , h, footbrake, handBrake, isJump);


		}

//		public void detectPressedKeyOrButton()
//		{
//			foreach(KeyCode kcode in Enum.GetValues(typeof(KeyCode)))
//			{
//				if (Input.GetKeyDown(kcode))
//					Debug.Log("KeyCode down: " + kcode);
//			}
//		}
	}
}