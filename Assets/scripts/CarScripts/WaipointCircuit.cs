﻿using System;
using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace scripts{
	
	public class WaipointCircuit : MonoBehaviour {

		[SerializeField]private List<Transform> Waypoints = new List<Transform>();
		[SerializeField]private bool smoothRoute = true;
		[SerializeField]private float editorVisualizationSubsteps = 100;
		[SerializeField]private float sizeWireSphere = 3.5f;

		private float[] distances;
		private Vector3[] points;
		private int numPoints;

		public float Length{ get; private set;}
		public List<Transform> wayPoints{ get { return Waypoints; }  }

		private int p0n;
		private int p1n;
		private int p2n;
		private int p3n;

		private float ii;
		private Vector3 p0;
		private Vector3 p1;
		private Vector3 p2;
		private Vector3 p3;

		Transform[] theArray;



		void Awake(){
			if(Waypoints.Count > 1){
				CachePositionAndDistances ();
			}
			numPoints = Waypoints.Count;
		}

		private void CachePositionAndDistances (){
		
			points = new Vector3[Waypoints.Count + 1];
			distances = new float[Waypoints.Count +1];

			float accumulateDistance = 0;
			for(int i = 0; i < points.Length; ++i ){
				var t1 = Waypoints[(i) % Waypoints.Count];
				var t2 = Waypoints[(i + 1) % Waypoints.Count];
				if(t1 != null && t2 != null){
					Vector3 p1 = t1.position;
					Vector3 p2 = t2.position;
					points[i] = Waypoints[i%Waypoints.Count].position;
					distances [i] = accumulateDistance;
					accumulateDistance += (p1 - p2).magnitude;
				}

			}
		}

		private Vector3 CatmullRom (Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float ii){
		
			return 0.5f * ((2 * p1) + (-p0 + p2) * ii + 
							(2 * p0 - 5 * p1 + 4 * p2 - p3) * ii* ii +
							(-p0 + 3 * p1 - 3 * p2 + p3) * ii * ii * ii);
		}


		public Vector3 GetRoutePosition(float dist){
		
			int point = 0;
			if(Length == 0)
				Length = distances[distances.Length - 1];

			dist = Mathf.Repeat (dist, Length);

			while (distances[point] < dist)
				++point;

			p1n = ((point - 1) + numPoints)%numPoints;
			p2n = point;

			ii = Mathf.InverseLerp (distances[p1n], distances[p2n], dist);

			if (smoothRoute) {

				p0n = ((point - 2) + numPoints) % numPoints;
				p3n = (point + 1) % numPoints;

				p2n = p2n % numPoints;

				p0 = points [p0n];
				p1 = points [p1n];
				p2 = points [p2n];
				p3 = points [p3n];

				return CatmullRom (p0, p1, p2, p3, ii);
			} else {
			
				p1n = ((point - 1) + numPoints)% numPoints;
				p2n = point;

				return Vector3.Lerp (points[p1n], points[p2n], ii);

			}


		}

		private void OnDrawGizmos(){
		
			DrawGizmos (false);
		}

		private void OnDrawGizmosSelected(){
		
			DrawGizmos (true);
		}

		void DrawGizmos(bool selected){


			theArray = GetComponentsInChildren<Transform>();
			Waypoints.Clear();

			//pega todos os elementos da pasta e joga po transforme no array
			foreach(Transform waypoint in theArray){

				if(waypoint != this.transform){

					Waypoints.Add (waypoint);
				}
			}

			if(Waypoints.Count > 1){

				numPoints = Waypoints.Count;

				CachePositionAndDistances ();
				Length = distances[distances.Length - 1];

				Gizmos.color = selected ? Color.yellow : new Color (1, 1, 0, 0.5f);
				Vector3 prev = Waypoints [0].position;

				if (smoothRoute) {
					for (float dist = 0; dist < Length; dist += Length / editorVisualizationSubsteps) {
						Vector3 next = GetRoutePosition (dist + 1);
						Gizmos.DrawLine (prev, next);
						prev = next;
					}
					Gizmos.DrawLine (prev, Waypoints [0].position);
				} else {
				
					for(int n = 0; n < Waypoints.Count; ++n){
						Vector3 next = Waypoints[(n + 1)% Waypoints.Count].position;
						Gizmos.DrawLine (prev, next);
						prev = next;
					}
				}


			}
				
			for(int i = 0; i < Waypoints.Count; i++){

				Vector3 position = Waypoints [i].position;
				if(i >= 0){
					//Vector3 previous = Waypoints [i - 1].position;
					//Gizmos.DrawLine (previous, position);
					Gizmos.DrawWireSphere (position, sizeWireSphere);
				}
			}


		}

		//======================================================================



		public struct RoutPoint{

			public Vector3 position;
			public Vector3 direction;

			public RoutPoint(Vector3 position, Vector3 direction){

				this.position = position;
				this.direction = direction;
			}
		}

		public RoutPoint GetRoutePoint(float dist)
		{
			Vector3 p1 = GetRoutePosition (dist);
			Vector3 p2 = GetRoutePosition (dist + 0.1f );
			Vector3 delta = p2 - p1;
			return new RoutPoint (p1, delta.normalized);
		}



	}
}