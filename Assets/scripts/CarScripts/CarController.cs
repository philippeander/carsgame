﻿// Esse script no geral é o motor do carro.

using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace scripts{


	internal enum SpeedType{

		MPH,
		KPH   
	}


	[Serializable]
	public class WheelIndividualConfiguration{
		//Essa classe é chamada como um ARRAY na classe abaixo ( carController ), e é responsavel por pegar e setar as informações de cada roda.
		public WheelCollider m_wheelCollider; 	//É a roda individual propriamente dita
		public Transform WheelMesh = null;		//A mesh relacionada a roda para sincronizar o movimento com o wheelColider acima
		public bool motor;						//Verifica se essa roda é um motor
		public bool steering;					//Verifica se nessa roda aplica o angulo de rotação
		public bool brake = true;				//Quais rodas usam o footbrake
		public bool handbrake = false;			//Quais rodas usam o Handbrake
		[NonSerialized]public bool isGrounded;	//Verifica se a roda está em contato com o chão, feito para minimizar a capotagem dos carros
	}


	public class CarController : MonoBehaviour {

		[SerializeField]private SpeedType m_SpeedType; //


		[Header("Wheels")]
		[SerializeField]private List<WheelIndividualConfiguration> WheelInfos;	//Lista de todas as rodas

		[Header("Wheels Config")]
		[SerializeField]private float m_MaxSteerAngle = 25;							// Agulo maximo de rotação da roda
		[SerializeField]private float m_MaxHandbrakeTorque;
		[SerializeField]private float m_BrakeTorque = 20000.0f;
		[SerializeField]private float m_ReverseTorque = 500.0f;
		[SerializeField]private float m_Downforce = 100f;
		[SerializeField]private float m_Topspeed = 200;
		[SerializeField]private static int NoOfGears = 5;
		[SerializeField]private float m_RevRangeBoundary = 1f;
		[SerializeField]private float m_SlipLimit = 0.3f;
		[SerializeField]private float jumpForce = 1000.0f;

		[Header("Rigidbody")]
		[SerializeField]private Rigidbody m_rigidbody;
		[SerializeField]private Vector3 m_centerOfMass;

		[Header("Speed Config")]
		[SerializeField]private float speed;


		[Header("Other")]
		[SerializeField]private MeshRenderer BeckLantern;

		private float m_SteerAngle;
		private float m_CurrentTorque;
		private int m_GearNum;
		private float m_GearFactor;
		private float m_OldRotation;

		[SerializeField]private int wheelCont = 0;

		[Range(0, 1)] [SerializeField] private float m_SteerHelper = 0.644f;
		[Range(0, 1)] [SerializeField] private float m_TractionControl = 1.0f;
		[SerializeField] private float m_FullTorqueOverAllWheels = 2500.0f;

		public float CurrentSpeed{ get { return m_rigidbody.velocity.magnitude*2.23693629f; }}
		public float MaxSpeed{get { return m_Topspeed; }}
		public float Revs { get; private set; }
		public int gearNo{ get{return m_GearNum;}}


		void Awake(){

			m_rigidbody = GetComponent<Rigidbody>();

		}
		// Use this for initialization
		void Start () {


			WheelInfos [0].m_wheelCollider.attachedRigidbody.centerOfMass = m_centerOfMass;

			m_MaxHandbrakeTorque = float.MaxValue;

			m_CurrentTorque = m_FullTorqueOverAllWheels - (m_TractionControl*m_FullTorqueOverAllWheels);

			foreach(WheelIndividualConfiguration wheelInfo in WheelInfos ){

				if (wheelInfo.motor)
					wheelCont++;
			}

//			GameObject pp =  Instantiate (pointPlayer, pointPlayer.transform.position, pointPlayer.transform.rotation) as GameObject;
//			pp.GetComponent<PointFollowPlayer> ().goPlayer = this.gameObject; 


		}
			
//		void Update () {
//
//		}
//
//		public void FixedUpdate () {
//
//
//		}

		public void Engine(float accelerate, float steering, float footbrake, float handbrake, bool jump){

			if(jump){
				Debug.Log ("JUMP ACTIVE!!!");
			}
			//Pega os valores de entrada
			accelerate = Mathf.Clamp (accelerate, 0, 1);
			footbrake = -1 * Mathf.Clamp (footbrake, -1, 0);

			steering = Mathf.Clamp (steering, -1, 1);
			handbrake = Mathf.Clamp (handbrake, 0, 1);

			//acende a lanterna trazeira quando freia.
			if (footbrake > 0 || handbrake > 0)
				BeckLantern.enabled = true;
			else
				BeckLantern.enabled = false;

			//Esse "FOR" é para gerenciar as funções de cada Roda, ele pega os valores e adiciona aos parametros dos *Metódos
			//Isso evita tá criando o mesmo for, varias vezes, em todos os metódos
			foreach(WheelIndividualConfiguration wheelInfo in WheelInfos ){
				

				ApplyLocalPositionVisuals (wheelInfo);
				ApplyDrive (wheelInfo, accelerate, footbrake);
				UpdateSteering(wheelInfo, steering);
				UpdateHandbrake (wheelInfo, handbrake);

				SteerHelper (wheelInfo);
				CapSpeed ();

				CalculateRevs ();
				GearChanging ();

				AddDownForce (jump);
				MyJump (jump);
				TractionControl (wheelInfo);

				AirMode (wheelInfo);


			}
				
		}

		//================================================================================



		void ApplyDrive(WheelIndividualConfiguration wheelUni, float accelerate, float footbrake){

			float thrustTorque;
			//Esse Script distribui a forca do motor igualmente para as rodas que o compõe
			//Para evitar que um 4x4 seja mais agil que um carro normal
			switch(wheelCont){

			case 4:
				thrustTorque = accelerate * (m_CurrentTorque / 4);
				if (wheelUni.motor) {
					
					wheelUni.m_wheelCollider.motorTorque = thrustTorque;
				}
				break;

			case 2:
				thrustTorque = accelerate * (m_CurrentTorque / 2);
				if (wheelUni.motor) {

					wheelUni.m_wheelCollider.motorTorque = thrustTorque;
				}
				break;

			}

			//Esse script é responsavel pelo footbrake
			//Se o botão continuar pressionado e a velocidade for igual a zero, o carro exerce velocidade contraria
			if (wheelUni.motor) {
			
				if(CurrentSpeed > 5 && Vector3.Angle(transform.forward, m_rigidbody.velocity) < 50f){

					wheelUni.m_wheelCollider.brakeTorque = m_BrakeTorque * footbrake;
				}else if(footbrake > 0){
					
					wheelUni.m_wheelCollider.brakeTorque = 0f;
					wheelUni.m_wheelCollider.motorTorque = -m_ReverseTorque * footbrake;

				}
			}


		}
			
		//Esse metodo é responsavel pela curva das rodas marcadas como sterring no ARRAY WheelInfos, no inspector
		void UpdateSteering(WheelIndividualConfiguration wheelUni, float steer){

			m_SteerAngle = steer * m_MaxSteerAngle;

			if(wheelUni.steering){

				wheelUni.m_wheelCollider.steerAngle = m_SteerAngle;
			}

		}

		//Esse método é responsavel pelo freio de mão, e funciona atraves das rodas também marcadas no Array no inspector
		void UpdateHandbrake(WheelIndividualConfiguration wheelUni, float handbrake){

			if(wheelUni.handbrake){
				if (handbrake > 0f) {
					
					var hbTorque = handbrake * m_MaxHandbrakeTorque;
					wheelUni.m_wheelCollider.brakeTorque = hbTorque;

				} 
			}

		}

		//================================================================================

		public void ApplyLocalPositionVisuals(WheelIndividualConfiguration wheelUni){

			Quaternion quat;
			Vector3 pos;
			//Sincroniza a wheelcolider com mesh relacionada a roda
			wheelUni.m_wheelCollider.GetWorldPose(out pos, out quat);

			if (wheelUni.WheelMesh != null) {
				
				wheelUni.WheelMesh.position = pos;
				wheelUni.WheelMesh.rotation = quat;
			}
		}

		private void GearChanging(){
			//muda a marcha do carro de acordo com as rotações do mmotor
			float f = Mathf.Abs (CurrentSpeed / MaxSpeed);
			float upGearLimit = (1 / (float)NoOfGears) * (m_GearNum + 1);
			float downGearLimit = (1 / (float)NoOfGears) * m_GearNum;

			if(m_GearNum > 0 && f < downGearLimit)
				m_GearNum--;
			if (f > upGearLimit && (m_GearNum < (NoOfGears - 1)))
				m_GearNum++;
			
		}

		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

		private void CalculateGearFactor(){

			float f = (1 / (float)NoOfGears);

			var targetGearFactor = Mathf.InverseLerp (f * m_GearNum, f * (m_GearNum +1), Mathf.Abs(CurrentSpeed/MaxSpeed));
			m_GearFactor = Mathf.Lerp (m_GearFactor, targetGearFactor, Time.deltaTime * 5f);

		}

		private static float ULerp(float from, float to, float value){
		
			return (1.0f - value) * from + value * to;
		}

		private static float CurveFactor(float factor){

			return 1 - (1 - factor) * (1 - factor);
		}

		private void CalculateRevs(){
			// Calcula as rotações do motor (para exibição / som)

			CalculateGearFactor ();
			var gearNumFactor = m_GearNum /(float)NoOfGears;
			var revsRangeMin = ULerp (0f, m_RevRangeBoundary, CurveFactor(gearNumFactor));
			var revsRangeMax = ULerp(m_RevRangeBoundary, 1f, gearNumFactor);
			Revs = ULerp (revsRangeMin, revsRangeMax, m_GearFactor);
		}

		//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

		private void AdjustTorque(float forwardSlip){
		
			if (forwardSlip >= m_SlipLimit && m_CurrentTorque >= 0) {

				m_CurrentTorque -= 10 * m_TractionControl;
			} else {
			
				m_CurrentTorque += 10 * m_TractionControl;
				if(m_CurrentTorque > m_FullTorqueOverAllWheels){

					m_CurrentTorque = m_FullTorqueOverAllWheels;
				}
			}
		}

		// Controle de tração bruto que reduz o poder da roda do carro girar demais
		private void TractionControl(WheelIndividualConfiguration wheelUni){
		
			WheelHit wheelHit;

			if (wheelUni.motor) {

				wheelUni.m_wheelCollider.GetGroundHit (out wheelHit);

				AdjustTorque(wheelHit.forwardSlip);
			}


		}

		//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

		private void SteerHelper(WheelIndividualConfiguration wheelUni){
		
			WheelHit wheelHit;

			wheelUni.m_wheelCollider.GetGroundHit (out wheelHit);

			if (wheelHit.normal == Vector3.zero)
				return; // rodas estão no chão para não realinhar a velocidade de corpo rígido

			// Isso, se for necessário para evitar problemas de bloqueio cardan que farão o carro de repente mudar de direção
			if(Mathf.Abs(m_OldRotation - transform.eulerAngles.y) < 10f){

				var turnAdjust = (transform.eulerAngles.y - m_OldRotation) * m_SteerHelper;
				Quaternion velRotation = Quaternion.AngleAxis (turnAdjust,Vector3.up);
				m_rigidbody.velocity = velRotation * m_rigidbody.velocity;
			}
			m_OldRotation = transform.eulerAngles.y;

		}

		private void CapSpeed(){
		
			float speed = m_rigidbody.velocity.magnitude;

			switch(m_SpeedType){

			case SpeedType.MPH:
				speed *= 2.23693629f;
				if (speed > m_Topspeed)
					m_rigidbody.velocity = (m_Topspeed / 2.23693629f) * m_rigidbody.velocity.normalized;
				break;

			case SpeedType.KPH:
				if (speed > m_Topspeed)
					m_rigidbody.velocity = (m_Topspeed / 3.6f) * m_rigidbody.velocity.normalized;
				break;
			}
		}

		private void AirMode(WheelIndividualConfiguration wheelUni){
		
			//Esse método faz com que quando o carro esteja no ar, ele tente realinhar para cair em pé
			WheelHit hit;
			wheelUni.isGrounded = wheelUni.m_wheelCollider.GetGroundHit (out hit);

			if (!WheelInfos [2].isGrounded)
				transform.rotation = Quaternion.Lerp (transform.rotation, Quaternion.FromToRotation (transform.up, Vector3.up) * transform.rotation, 0.02f);

		}

		// Adiciona mais controle em relação a velocidade
		private void AddDownForce(bool jump){
			if(!jump)
				WheelInfos [0].m_wheelCollider.attachedRigidbody.AddForce (-transform.up * m_Downforce * WheelInfos [0].m_wheelCollider.attachedRigidbody.velocity.magnitude);
		}

		private void MyJump(bool jump){
			if (jump && WheelInfos[2].isGrounded)
				m_rigidbody.AddForce (Vector3.up * jumpForce, ForceMode.Impulse ); 
		}




		//----------------------------------




	}
}