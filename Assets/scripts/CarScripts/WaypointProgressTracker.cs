﻿using UnityEngine;
using System.Collections;

namespace scripts{

	internal enum ProgressStile{

		SmoothLongRout,
		PointToPoint
	}
	//[RequireComponent(typeof(WaipointCircuit))]
	public class WaypointProgressTracker : MonoBehaviour {

		[SerializeField]private WaipointCircuit circuit;

		[SerializeField]private float lookAheadForTargetOffset = 5f;
		[SerializeField]private float lookAheadForTargetFactor = 0.1f;

		[SerializeField]private float lookAheadForSpeedOffset = 10f;
		[SerializeField]private float lookAheadForSpeedFactor = 0.2f;

		[SerializeField]private ProgressStile progressStyle = ProgressStile.SmoothLongRout;
		[SerializeField]private float pointToPointThreshold = 4;

		public WaipointCircuit.RoutPoint targetPoint{ get; private set;}
		public WaipointCircuit.RoutPoint speedPoint{ get; private set;}
		public WaipointCircuit.RoutPoint progressPoint{ get; private set;}

		public Transform target;

		private float progressDistance;
		private int progressNum;
		private Vector3 lastPosition;
		private float speed;

		void Awake(){
			GameObject Wp = GameObject.Find ("Waypoints");
			circuit = Wp.GetComponent<WaipointCircuit> ();
		}

		void Start(){

			if (target == null)
				target = new GameObject (name + "Waypoint Target").transform;

			Reset ();
		}

		void Update(){
			if (progressStyle == ProgressStile.SmoothLongRout) {
				if (Time.deltaTime > 0)
					speed = Mathf.Lerp (speed, (lastPosition - transform.position).magnitude / Time.deltaTime, Time.deltaTime);

				target.position = circuit.GetRoutePoint (progressDistance + lookAheadForTargetOffset +
				lookAheadForTargetFactor * speed).position;
				target.rotation = Quaternion.LookRotation (circuit.GetRoutePoint (progressDistance +
				lookAheadForSpeedOffset + lookAheadForSpeedFactor * speed).direction);


				progressPoint = circuit.GetRoutePoint (progressDistance);
				Vector3 progressDelta = progressPoint.position - transform.position;
				if (Vector3.Dot (progressDelta, progressPoint.direction) < 0)
					progressDistance += progressDelta.magnitude * 0.5f;

				lastPosition = transform.position;

			} else {
				Vector3 targetDelta = target.position - transform.position;
				if(targetDelta.magnitude < pointToPointThreshold)
					progressNum = (progressNum +1) % circuit.wayPoints.Count;

				target.position = circuit.wayPoints [progressNum].position;
				target.rotation = circuit.wayPoints [progressNum].rotation;

				progressPoint = circuit.GetRoutePoint (progressDistance);
				Vector3 progressDelta = progressPoint.position - transform.position;
				if (Vector3.Dot (progressDelta, progressPoint.direction) < 0)
					progressDistance += progressDelta.magnitude;

				lastPosition = transform.position;

			}
		}

		public void Reset(){
			progressDistance = 0;
			progressNum = 0;
			if(progressStyle == ProgressStile.PointToPoint){
				target.position = circuit.wayPoints [progressNum].position;
				target.rotation = circuit.wayPoints [progressNum].rotation;
			}
		}

		private void OnDrawGizmos(){
			if(Application.isPlaying){
				Gizmos.color = Color.green;
				Gizmos.DrawLine(transform.position, target.position);
				Gizmos.DrawWireSphere (circuit.GetRoutePosition(progressDistance), 1);
				Gizmos.color = Color.yellow;
				Gizmos.DrawLine (target.position, target.position + target.forward);
			}
		}


	}
}