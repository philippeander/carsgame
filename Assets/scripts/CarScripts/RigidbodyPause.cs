﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace scripts{

	[RequireComponent(typeof(Rigidbody))]
	public class RigidbodyPause : MonoBehaviour
	{
		//public bool pause = false;
		public KeyCode key = KeyCode.R;


		bool m_pausedState = false;
		Vector3 m_velocity = Vector3.zero;
		Vector3 m_angularVelocity = Vector3.zero;

		// Enabling / disabling vehicle and wheelcolliders should be unnecesary in Unity 5.2+
		// (patch pending)

		CarController m_vehicle; 
		Rigidbody m_rigidbody;

		void Awake(){
			
		}

		void OnEnable ()
		{
			m_rigidbody = GetComponent<Rigidbody>();
			m_vehicle = GetComponent<CarController>();
		}


		void FixedUpdate ()
			{
//			if (Countdown.rbPause && !m_pausedState)
//				{
//				m_velocity = m_rigidbody.velocity;
//				m_angularVelocity = m_rigidbody.angularVelocity;
//
//				m_pausedState = true;
//				m_rigidbody.isKinematic = true;
//
//				if (m_vehicle)
//					{
//					m_vehicle.enabled = false;
//					DisableWheelColliders();
//					}
//				}
//			else
//				if (!Countdown.rbPause && m_pausedState)
//				{
//				m_rigidbody.isKinematic = false;
//
//				if (m_vehicle)
//					{
//					EnableWheelColliders();
//					m_vehicle.enabled = true;
//					}
//
//				m_rigidbody.AddForce(m_velocity, ForceMode.VelocityChange);
//				m_rigidbody.AddTorque(m_angularVelocity, ForceMode.VelocityChange);
//
//				m_pausedState = false;
//				}
				
			if (Countdown.rbPause && !m_pausedState)
			{
				
				m_pausedState = true;

				if (m_vehicle)
				{
					m_vehicle.enabled = false;
					DisableWheelColliders();
				}
			}
			else
				if (!Countdown.rbPause && m_pausedState)
				{

					if (m_vehicle)
					{
						EnableWheelColliders();
						m_vehicle.enabled = true;
					}
					m_pausedState = false;
				}


			}


		void DisableWheelColliders ()
			{
//			WheelCollider[] colliders = GetComponentsInChildren<WheelCollider>();
//
//			foreach (WheelCollider wheel in colliders)
//				wheel.enabled = false;
			m_rigidbody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
			}


		void EnableWheelColliders ()
			{
//			WheelCollider[] colliders = GetComponentsInChildren<WheelCollider>();
//
//			foreach (WheelCollider wheel in colliders)
//				wheel.enabled = true;
			m_rigidbody.constraints = RigidbodyConstraints.None;
			}


		void Update ()
		{
			if (Input.GetKeyDown(key)) Countdown.rbPause = !Countdown.rbPause;
		}


	}
}
